package com.prueba.ejercicio1;

public interface EmpleadoControl {
    boolean estaDistraido();

    void setDistraido(boolean distraido);
}

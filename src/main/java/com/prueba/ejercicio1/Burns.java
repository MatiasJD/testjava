package com.prueba.ejercicio1;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
public class Burns {
    private boolean esPobre;

    public boolean estaPobre(){
        return esPobre;
    }

    public void setEsPobre(boolean esPobre) {
        this.esPobre = esPobre;
    }
}

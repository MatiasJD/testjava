import com.prueba.ejercicio2.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TestEjercicio2 {
    public EmpleadoTablaDB empleadoTablaDB;
    public Metodos metodos;

    @Before
    public void crearEmpleado(){
        empleadoTablaDB = new EmpleadoTablaDB("","", null,"empleadoBueno","empleadoMalo");
        metodos = new Metodos();
    }

    @Test
    public void empleadoNuevo(){
        empleadoTablaDB.setTipo(null);
        Assert.assertTrue(metodos.obtenerEmpleadoRefactor(empleadoTablaDB) instanceof EmpleadoNuevo);
    }

    @Test
    public void empleadoBueno(){
        empleadoTablaDB.setTipo(TipoEmpleado.BUENO);
        Assert.assertTrue(metodos.obtenerEmpleadoRefactor(empleadoTablaDB) instanceof EmpleadoBueno);
    }

    @Test
    public void empleadoMalo(){
        empleadoTablaDB.setTipo(TipoEmpleado.MALO);
        Assert.assertTrue(metodos.obtenerEmpleadoRefactor(empleadoTablaDB) instanceof EmpleadoMalo);
    }
}

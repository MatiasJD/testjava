package com.prueba.ejercicio2;

public class Metodos {
    public Empleado obtenerEmpleado(EmpleadoTablaDB empleadoDB){
        try {
            if (empleadoDB.getTipo().toString() == "MALO") {
                EmpleadoMalo homero = new EmpleadoMalo();
                homero.setNombre(empleadoDB.getNombre());
                homero.setApellido(empleadoDB.getApellido());
                homero.setSoloEmpleadoMalo(empleadoDB.getSoloEmpleadoMalo());
                return homero;
            }else if(empleadoDB.getTipo().toString() == "BUENO"){
                EmpleadoBueno smithers = new EmpleadoBueno();
                smithers.setNombre(empleadoDB.getNombre());
                smithers.setApellido(empleadoDB.getApellido());
                smithers.setSoloEmpleadoBueno(empleadoDB.getSoloEmpleadoBueno());
                return smithers;
            }
        } catch (Exception e) {
            //Si viene un empleado nuevo el "tipo" es NULL y explota cuando hago .toString(),
            //Que trucazo, no?
            EmpleadoNuevo nuevo = new EmpleadoNuevo();
            nuevo.setNombre(empleadoDB.getNombre());
            nuevo.setApellido(empleadoDB.getApellido());
            return nuevo;
        }
        return null;
    }

    public Empleado obtenerEmpleadoRefactor(EmpleadoTablaDB empleadoDB){
        Empleado empleado = null;
        if(empleadoDB.getTipo() == null){
            empleado = new EmpleadoNuevo();
        }else if (empleadoDB.getTipo().toString() == "MALO") {
            empleado = new EmpleadoMalo();
            ((EmpleadoMalo) empleado).setSoloEmpleadoMalo(empleadoDB.getSoloEmpleadoMalo());
        }else if(empleadoDB.getTipo().toString() == "BUENO"){
            empleado = new EmpleadoBueno();
            ((EmpleadoBueno) empleado).setSoloEmpleadoBueno(empleadoDB.getSoloEmpleadoBueno());
        }
        empleado.setNombre(empleadoDB.getNombre());
        empleado.setApellido(empleadoDB.getApellido());
        return empleado;
    }
}

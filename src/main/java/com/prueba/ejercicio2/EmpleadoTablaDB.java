package com.prueba.ejercicio2;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
public class EmpleadoTablaDB {
    private String nombre;
    private String apellido;
    private TipoEmpleado tipo;
    private String soloEmpleadoBueno;
    private String soloEmpleadoMalo;
}

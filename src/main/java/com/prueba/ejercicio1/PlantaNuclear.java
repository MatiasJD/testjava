package com.prueba.ejercicio1;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PlantaNuclear {
    private EmpleadoControl empleadoControl;
    private int cantidadUranio;
    private Burns duenio;

    public void agregarUranio(int cantidadUranio){
        this.cantidadUranio += cantidadUranio > 0 ? cantidadUranio : 0;
    }

    public boolean estaEnPeligro(){
        if((cantidadUranio > 10000 && empleadoControl.estaDistraido()) || duenio.estaPobre()){
            return true;
        }
        return false;
    }



}

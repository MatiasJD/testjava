import com.prueba.ejercicio1.*;
import org.junit.Assert;
import org.junit.Test;

public class TestEjecicio1 {

    @Test
    public void testEstaEnPeligro(){
        EmpleadoControl homero = new Homero(false, 0);
        Burns burns = new Burns(true);
        PlantaNuclear plantaNuclear = new PlantaNuclear(homero, 10000, burns);
        Assert.assertTrue(plantaNuclear.estaEnPeligro());

        plantaNuclear.getDuenio().setEsPobre(false);
        plantaNuclear.getEmpleadoControl().setDistraido(true);
        plantaNuclear.agregarUranio(10000);
        Assert.assertTrue(plantaNuclear.estaEnPeligro());

        EmpleadoControl patoBalancin = new PatoBalancin(true);
        plantaNuclear.setEmpleadoControl(patoBalancin);
        Assert.assertTrue(plantaNuclear.estaEnPeligro());
    }

    @Test
    public void testNoEstaEnPeligro(){
        EmpleadoControl empleadoControl = new Homero(false, 0);
        Burns burns = new Burns(false);
        PlantaNuclear plantaNuclear = new PlantaNuclear(empleadoControl, 10000, burns);
        Assert.assertFalse(plantaNuclear.estaEnPeligro());

        empleadoControl.setDistraido(true);
        Assert.assertFalse(plantaNuclear.estaEnPeligro());

        EmpleadoControl patoBalancin = new PatoBalancin(false);
        plantaNuclear.setEmpleadoControl(patoBalancin);
        plantaNuclear.agregarUranio(10000);
        Assert.assertFalse(plantaNuclear.estaEnPeligro());
    }
}

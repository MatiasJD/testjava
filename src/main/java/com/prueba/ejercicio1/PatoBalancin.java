package com.prueba.ejercicio1;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
public class PatoBalancin implements EmpleadoControl{
    private boolean distraido;

    public boolean estaDistraido() {
        return distraido;
    }

    public void setDistraido(boolean distraido) {
        this.distraido = distraido;
    }
}

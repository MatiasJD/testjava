package com.prueba.ejercicio2;

import lombok.Data;

@Data
public class EmpleadoMalo extends Empleado {
    private String soloEmpleadoMalo;
}

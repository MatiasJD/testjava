package com.prueba.ejercicio1;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
public class Homero implements EmpleadoControl{
    private boolean distraido;
    private int donas;

    public boolean estaDistraido() {
        return distraido;
    }

    public void setDistraido(boolean distraido){
        this.distraido = distraido;
    }

    public void comprarDonas(int donas){
        this.donas += donas > 0 ? donas : 0;
    }

    public void comerDona(){
        donas -= donas > 0 ? 1 : 0;
    }

    public int getDonas(){
        return donas;
    }
}

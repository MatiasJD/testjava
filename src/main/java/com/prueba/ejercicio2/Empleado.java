package com.prueba.ejercicio2;

import lombok.Data;

@Data
public abstract class Empleado {
    private String nombre;
    private String apellido;
}

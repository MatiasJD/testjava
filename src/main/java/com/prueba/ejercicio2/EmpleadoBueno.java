package com.prueba.ejercicio2;

import lombok.Data;

@Data
public class EmpleadoBueno extends Empleado {
    private String soloEmpleadoBueno;
}
